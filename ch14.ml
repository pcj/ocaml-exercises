module type STACK =
sig
  type 'a t
  exception Empty
  val create : unit -> 'a t
  val push : 'a -> 'a t -> unit
  val pop : 'a t -> 'a
  val clear : 'a t -> unit
  val length : 'a t -> int
  val iter : ('a -> unit) -> 'a t -> unit
end

let range n =
  let rec range_aux n l =
    if n < 0 
    then l 
    else range_aux (n - 1) (n :: l) in
  range_aux (n - 1) []

module MyStack : STACK =
struct
  type 'a t = {mutable sp : int; mutable c : 'a array}
  exception Empty
  let increase s e = 
    let a = Array.make (Array.length s.c * 2 + 1) e in
    Array.blit s.c 0 a 0 (Array.length s.c) ;
    s.c <- a
  let push e s = 
    begin
      if (s.sp = Array.length s.c) then increase s e else (); 
      s.c.(s.sp) <- e; 
      s.sp <- (s.sp + 1)
    end
  let pop s = 
    if s.sp = 0 
    then raise Empty 
    else s.sp <- (s.sp - 1) ; s.c.(s.sp)
  let clear s = 
    s.sp <- 0; s.c <- [||]
  let length s = s.sp
  let create () = {sp = 0; c = [||]}
  let iter f s =
    let b = s.sp - 1 in
    for i = 0 to b
    do
      f (s.c.(b - i))
    done
end    

module type INT = 
sig
  type t
  exception Isnul
  val of_int : t -> t
  val mult : t -> t -> t
end

module Int_star : INT =
struct
  type t = int
  exception Isnul
  let of_int = function 0 -> raise Isnul | n -> n
  let mult = ( * )
end

module type S = 
sig
  type t
  val create : unit -> t
  val add : t -> unit
  val get : t -> int
end

module M : S = 
struct
  type t = int ref
  let create () = ref 0
  let add x = incr x
  let get x = if !x > 0 then (decr x; !x) else failwith "Empty"
end

module type S1 = 
sig
  type t
  val create : unit -> t
  val add : t -> unit
end

module type S2 = 
sig 
  type t
  val get : t -> int
end

module M1 = (M : S1 with type t = M.t)
module M2 = (M : S2 with type t = M.t)

module OrderedInt : Set.OrderedType =
struct
  type t = int * int
  let compare (x1, y1) (x2, y2) = 
    if x1 > x2 then 1
    else if x1 < x2 then -1
    else if y1 > y2 then 1
    else if y1 < y2 then -1
    else 0
end

