open Unix

(* 
   The funtion call 
   
   find handler action follow depths roots

   traverses the file hierarchy starting from roots specified in the list __roots__ (absolute or
   relative to the current working directory of the process when the call is made) up to a
   maximum of depth __depth__ and follow the symbolic links if flag __follow__ is set.
   The paths found under root r include r as a prefix. Each found path p is given to the function
   __action__ along with the data returned by Unix.lstat p (or Unix.stat p if follow is true). 
   The function __action__ returns a boolean indicating, for directories, whether the search 
   should continue for its contents (true) or not (false).

   The __handler__ function reports traversal errors of type Unix.error. Whenever an error occurs
   the arguments of the exception are given to the __handler__ function and the traversal continues.
   However, when an exception is raised by function __action__ or the __handler__ themselves, we
   immediately stop the traversal and let it propogate to the caller.
*)

exception Hidden of exn

let hide_exn f x = try f x with exn -> raise (Hidden exn)
let reveal_exn f x = try f x with Hidden exn -> raise exn

let find on_error on_path follow depth roots =
  let rec find_rec depth visiting filename =
    try
      let infos = (if follow then stat else lstat) filename in
      let continue = hide_exn (on_path filename) infos in
      let id = infos.st_dev, infos.st_ino in
      if infos.st_kind = S_DIR && depth > 0 && continue && (not (List.mem id visiting))
      then 
        let process_child child = 
          if (child <> Filename.current_dir_name &&
                child <> Filename.parent_dir_name)
          then 
            let child_name = Filename.concat filename child in
            let visiting = id :: visiting in
            find_rec (depth - 1) visiting child_name in
        Misc.iter_dir process_child filename
    with Unix_error (e, b, c) -> hide_exn on_error (e, b, c) in
  reveal_exn (List.iter (find_rec depth [])) roots
          
    
let main () =
  let follow = ref false in
  let max_depth = ref max_int in
  let roots = ref [] in
  let usage_string = 
    ("Usage: " ^ Sys.argv.(0) ^ " [options...] [files...]") in
  let opt_list = [
    "-maxdepth", Arg.Int ((:=) max_depth), "max depth search";
    "-follow", Arg.Set follow, "follow symbolic links"] in
  Arg.parse opt_list (fun f -> roots := f :: !roots) usage_string;
  let action p infos = print_endline p; true in
  let errors = ref false in
  let on_error (e, b, c) = 
    errors := true; prerr_endline (c ^ ": " ^ Unix.error_message e) in
  find on_error action !follow !max_depth
    (if !roots = [] 
     then [Filename.current_dir_name]
     else List.rev !roots);
  if !errors then exit 1

let _ = Unix.handle_unix_error main ()
