let rec restart_on_EINTR f x =
  try f x 
  with Unix.Unix_error(Unix.EINTR, _, _) -> restart_on_EINTR f x

      
let handle_unix_error f x =
  try (restart_on_EINTR f x)
  with Unix.Unix_error(err, fun_name, arg) -> 
    prerr_string Sys.argv.(0);
    prerr_string ": \"";
    prerr_string fun_name;
    prerr_string "\" failed";
    if String.length arg > 0 then begin
      prerr_string " on \"";
      prerr_string arg;
      prerr_string "\""
    end;
    prerr_string ": ";
    prerr_endline (Unix.error_message err);
    exit 2

let try_finalize f x finally y = 
  try f x 
  with exn -> ignore (finally y); raise exn

let iter_dir f dirname = 
  let d = Unix.opendir dirname in
  try
    while true do
      ignore (f (Unix.readdir d))
    done
  with End_of_file -> Unix.closedir d
