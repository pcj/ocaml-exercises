let debug = ref false

let triple_sum a = 
  let n = (Array.length a - 1) in
  let count = ref 0 in
  for i = 0 to n do
    for j = (i + 1) to n do
      for k = (j + 1) to n do
        if ((a.(i) + a.(j) + a.(k)) = 0) then
          count := !count + 1
      done
    done
  done;
  !count

let triple_sum_fast a =
  let n = Array.length a - 1 in
  let count = ref 0 in
  let cmp x y = if x > y 
    then 1 
    else if x < y then -1 
    else 0 in
  begin
    Array.fast_sort cmp a;
    for i = 0 to n do
      for j = i + 1 to n do
        let s = a.(i) + a.(j) in
        let k = Util.binary_search a (-s) in
        if k > j
        then 
          begin
            count := !count + 1; 
            if !debug 
            then
              begin
                Printf.printf "a[%d] = %d a[%d] = %d a[%d] = %d" i a.(i) 
                  j a.(j) k  a.(k) ; 
                print_newline ()
              end
          end
        else ()
      done
    done;
    !count
  end
    

let main n =
  let n = (if !Sys.interactive then n else int_of_string Sys.argv.(1)) in
  let a = Array.make (n * 1024) 0 in
  begin
    for i = 0 to Array.length a - 1 do
      a.(i) <- Util.random_int 1_000_000
    done;
    let time_taken, result = Util.time_it triple_sum_fast a in
    Printf.printf "result = %d time_taken = %f\n" result time_taken;
    print_newline ();
    (* let time_taken, result = Util.time_it triple_sum a in *)
    (* Printf.printf "result = %d time_taken = %f" result time_taken; *)
    (* print_newline (); *)
  end
    
let _ = main 1
