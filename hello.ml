(* Hello, World *)

open Greet

let _ = 
  let name = (if Array.length Sys.argv > 1 then Sys.argv.(1) else "stranger") in
  begin
    if name = "Caesar" then greet Nicely name else greet Badly name;
    Printf.printf "My name is %s\n" Sys.argv.(0)
  end


    
  
