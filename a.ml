let a = Stk.create ()

let main () =
  for i = 0 to 10 
  do
    Stk.push i a
  done;
  Stk.iter (fun i -> print_string ((string_of_int i) ^ " ")) a;
  print_newline ()

let _ = main (); exit 0
