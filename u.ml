let read_lines in_c =
  let rec read_lines_aux lst =
    try
      let line = input_line in_c in
      read_lines_aux (line :: lst)
    with End_of_file -> lst
  in read_lines_aux []
    
    
let ls () =
  let (in_c, out_c, err_c) = Unix.open_process_full "ssh ubuntu 'uname -a'" (Unix.environment ()) in
  let lines = List.rev (read_lines in_c) in
  let err_lines = List.rev (read_lines err_c) in
  begin
    List.iter (fun i -> print_string "--- "; print_string i; print_newline ()) lines;
    List.iter (fun i -> print_string "--- "; print_string i; print_newline ()) err_lines;
    ignore (Unix.close_process_full (in_c, out_c, err_c))
  end

let _ = ls ()
            

