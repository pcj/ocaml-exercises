(* doubly linked list *)

type 'a cell = { info : 'a;
                 mutable prev : 'a dlist;
                 mutable next : 'a dlist }
and 'a dlist = Empty | List of 'a cell

let add elt lst = match lst with
  | Empty -> List {info = elt; prev = Empty; next = Empty}
  | List cell -> 
    let new_lst = List {info = elt; prev = Empty; next = List cell} in
    begin
      cell.prev <- new_lst; 
      new_lst
    end
    
let rec len = function 
  | Empty -> 0
  | List cell -> 1 + len cell.next

let rec print_int_dlist = function
  | Empty -> print_newline ()
  | List cell -> print_int cell.info; print_string"; "; print_int_dlist cell.next

let flip f x y = f y x

let a = List.fold_left (flip add) Empty [1; 2; 3; 4; 5; 6]

let b = List.fold_right add [1; 2; 3; 4; 5; 6] Empty

let remove_cell = function
  | Empty -> failwith "remove_cell: already empty"
  | List cell -> 
    match (cell.prev, cell.next) with
      | (Empty, Empty) -> Empty
      | (Empty, List c) -> c.prev <- Empty; cell.next
      | (List c, Empty) -> c.next <- Empty; cell.prev
      | (List c1, List c2) -> c1.next <- cell.next; c2.prev <- cell.prev; cell.prev

