#!/bin/bash

ocamlbuild -classic-display triple_sum.byte
ocamlbuild -classic-display triple_sum.native
ocamlbuild -classic-display -lib unix echo.byte
ocamlbuild -classic-display -lib unix echo.native
ocamlbuild -classic-display main.byte
ocamlbuild -classic-display main.native
ocamlbuild -classic-display -lib unix find.byte
ocamlbuild -classic-display -lib unix find.native
ocamlbuild -classic-display test_stk.byte
ocamlbuild -classic-display test_stk.native
ocamlbuild -classic-display test_que.byte
ocamlbuild -classic-display test_que.native

